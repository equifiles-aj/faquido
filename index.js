import React from 'react';
import classNames from 'classnames';

class QuestionAnswer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: true
    };
  }

  toggle = () => {
    let isOpen = this.state.isOpen;

    this.setState({
      isOpen: !isOpen
    });
  };

  render() {
    if (!this.state) return null;

    const questionCssClasses = classNames({
      faquido__question: true,
      'faquido__question--open': this.state.isOpen
    });

    const answerBlockCssClasses = classNames({
      'faquido__answer-block': true,
      'faquido__answer-block--open': this.state.isOpen
    });

    return (
      <div className="faquido__qa" key={this.props.id}>
        <div className={questionCssClasses} onClick={this.toggle}>
          <div className="faquido__question-title">{this.props.question}</div>
          <div className="faquido__question-opener">
            {!this.state.isOpen && <div className="icon__faq-open" />}
            {this.state.isOpen && <div className="icon__faq-close" />}
          </div>
        </div>

        <div className={answerBlockCssClasses}>
          {this.props.answer && <div className="faquido__answer">{this.props.answer}</div>}
          <div className="faquido__more">{this.props.more}</div>
        </div>
      </div>
    );
  }
}

class Faquido extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      lang: props.lang || props.faqs.config.defaultLanguage,
      keywords: null,
      category: props.cat || props.faqs.config.defaultCategory
    };
  }

  applySelection = data => {
    const cat = data.currentTarget.value;

    this.setState({
      category: cat
    });
  };

  renderCatFilter() {
    const selOptions = Object.keys(this.props.faqs.config.categories).map((cat, idx) => {
      return (
        <option value={cat} key={idx}>
          {this.props.faqs.config.categories[cat][this.state.lang]}
        </option>
      );
    });

    return (
      <div className="faquido__catfilter">
        Kategorie:&nbsp;
        <select onChange={this.applySelection} defaultValue={this.state.category}>
          {selOptions}
        </select>
      </div>
    );
  }

  applyKeywordFilter = e => {
    const keywords = e.currentTarget.value;
    this.setState({ keywords: keywords });
  };

  renderKeywordsFilter() {
    return (
      <div className="faquido__keywordsfilter">
        Keywords:&nbsp;
        <input type="text" onChange={this.applyKeywordFilter} />
      </div>
    );
  }

  renderSearchBar() {
    const config = this.props.faqs.config;

    if (!config.searchable) return null;

    const catFilter = config.filterCategory ? this.renderCatFilter() : null;
    const keywordsFilter = config.filterKeywords ? this.renderKeywordsFilter() : null;

    return (
      <div className="faquido__searchbar">
        {keywordsFilter} {catFilter}
      </div>
    );
  }

  matchesKeywords = (keywords, ques, answ, more) => {
    const kws = this.state.keywords.toLowerCase().split(/[^A-Za-z0-9]/);

    return kws.reduce((acc, el) => {
      if (acc !== true && `${ques} ${answ} ${more}`.toLowerCase().indexOf(el) !== -1) {
        acc = true;
      }
      return acc;
    }, false);
  };

  renderFaqList() {
    const faqData = this.props.faqs;

    const faqs = faqData.data.reduce((acc, block, idx) => {
      if (this.state.category === '*' || (block.cat && this.state.category === block.cat)) {
        const question = block.qa[this.state.lang].ques;
        const answer = block.qa[this.state.lang].answ;
        const more = block.qa[this.state.lang].more;

        if (
          this.state.keywords &&
          !this.matchesKeywords(this.state.keywords, question, answer, more)
        ) {
          return acc;
        }

        if (question && (answer || more)) {
          acc.push(
            <QuestionAnswer
              id={block.id || idx}
              key={idx}
              question={question}
              answer={answer}
              more={more}
            />
          );
        }
      }

      return acc;
    }, []);

    return <div className="faquido__index">{faqs}</div>;
  }

  render() {
    if (!this.state) {
      return null;
    }

    let faqs = this.renderFaqList();
    let searchBar = this.renderSearchBar();

    return (
      <div className="faquido">
        <h3>FAQs</h3>
        {searchBar}
        {faqs}
      </div>
    );
  }
}

export default Faquido;
