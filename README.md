# Faquido - react component for faq area

## Installation

1. Change to your components subdirectory

```
cd ./components/composites
```
2. Add the component directly as a submodule into your project
```
git submodule add https://equifiles-aj@bitbucket.org/equifiles-aj/faquido.git faquido
```
3. Include the component into your application
```
import Faquido from '../components/composites/faquido';
```
4. Include the component's css into your application stylesheet

Depending on your build system you can link the css file either in your main template via style tag or using the @link tag in your css file or you can directly import it like a javascript moodule if you use webpack css loader.

```
import faquidoCss from '../components/composites/faquido/index.css';
```
